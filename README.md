## OneWire

Simple platform-independent implementation of 1-Wire. Supports sync and async API. Uses sync or async calls for exchange with USART.

[**Example with STM32F030F4 MCU.**](Firmware/OneWireApplication)    

|API                                          |Sync                 |Async                        |Description                                                               |
|---------------------------------------------|---------------------|-----------------------------|--------------------------------------------------------------------------| 
|[**OneWire**](Firmware/OneWire/onewire.h)    |OW_Execute           |OW_BeginExecute              |Executes one of available 1-Wire commands.                                |
|                                             |                     |OW_CompleteExecute           |                                                                          |
|                                             |                     |                             |                                                                          |
|[**OneWire HAL**](Firmware/OneWire/onewire.h)|OW_HAL_ToInitSpeed   |OW_HAL_ToInitSpeed           |Switches USART speed to 9600.                                             |
|                                             |                     |                             |                                                                          |
|                                             |OW_HAL_ToWorkSpeed   |OW_HAL_ToWorkSpeed           |Switches USART speed to 115200.                                           |
|                                             |                     |                             |                                                                          |
|                                             |OW_HAL_Exchange      |OW_HAL_BeginExchange         |Exchange via USART.                                                       |
|                                             |                     |OW_HAL_EndExchange           |                                                                          |
|                                             |                     |                             |                                                                          |
|[**DS18B20**](Firmware/OneWire/ds18b20.h)    |DS18B20_ReadAddress  |DS18B20_BeginReadAddress     |Reads address from connected thermometer (executes *Read Rom* command).   |
|                                             |                     |DS18B20_CompleteReadAddress  |                                                                          |
|                                             |                     |                             |                                                                          |
|                                             |DS18B20_RunConversion|DS18B20_BeginRunConversion   |Runs temperature conversion (executes *Convert T* command).               |
|                                             |                     |DS18B20_CompleteRunConversion|                                                                          |
|                                             |                     |                             |                                                                          |
|                                             |DS18B20_ReadData     |DS18B20_BeginReadData        |Reads data from thermometer (executes *Read Scratchpad* command).         |
|                                             |                     |DS18B20_CompleteReadData     |                                                                          |
|                                             |                     |                             |                                                                          |
|                                             |DS18B20_WriteData    |DS18B20_BeginWriteData       |Writes configuration to thermometer (executes *Write Scratchpad* command).|
|                                             |                     |DS18B20_CompleteWriteData    |                                                                          |
|                                             |                     |                             |                                                                          |
|                                             |DS18B20_LoadData     |DS18B20_BeginLoadData        |Loads settings from embedded EEPROM (executes *Recall E2* command).       |
|                                             |                     |DS18B20_CompleteLoadData     |                                                                          |
|                                             |                     |                             |                                                                          |
|                                             |DS18B20_SaveData     |DS18B20_BeginSaveData        |Saves settings to embedded EEPROM (executes *Copy Scratchpad* command).   |
|                                             |                     |DS18B20_CompleteSaveData     |                                                                          |