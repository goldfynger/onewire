#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include "stm32f0xx_hal.h"
#include "onewire.h"
#include "ds18b20.h"
#include "onewire_app.h"


#define OW_APP_SYNC
#define OW_APP_ASYNC

#define OW_APP_USE_ADDRESS
//#define OW_APP_USE_LOAD_SAVE


void OW_APP_Sync(void);
void OW_APP_Async(void);


static DS18B20_HandleTypeDef _ds18b20Handle;

static volatile int32_t _temperature;


void OW_APP_Loop(void)
{
    while (true)
    {
        OW_APP_Sync();
        
        OW_APP_Async();
        
        HAL_Delay(1000); /* Wait before repeat. */
    }
}

void OW_APP_Sync(void)
{
#ifdef OW_APP_SYNC
    
    memset(&_ds18b20Handle, 0, sizeof(DS18B20_HandleTypeDef));
    
    OW_AddressTypeDef *pAddress = NULL;    
    
    OW_ErrorTypeDef error = OW_ERR_OK;

    
#ifdef OW_APP_USE_ADDRESS
    
    OW_AddressTypeDef address = {0};
    pAddress = &address;
    
    error = DS18B20_ReadAddress(&_ds18b20Handle, pAddress, NULL);
    
    if (error != OW_ERR_OK)
    {
        return;
    }
    
#endif
    
    DS18B20_DataTypeDef data =
    {
        .Temperature = 0,        
        .AlarmTriggerH = DS18B20_MAX_ALARM_TRIGGER_H,
        .AlarmTriggerL = DS18B20_MIN_ALARM_TRIGGER_L,        
        .Resolution = DS18B20_RESOLUTION_12
    };
        
    error = DS18B20_WriteData(&_ds18b20Handle, pAddress, &data, NULL);
    
    if (error != OW_ERR_OK)
    {
        return;
    }
    
#ifdef OW_APP_USE_LOAD_SAVE
    
        error = DS18B20_SaveData(&_ds18b20Handle, pAddress, NULL);
        
        if (error != OW_ERR_OK)
        {
            return;
        }
        
        HAL_Delay(DS18B20_SAVE_TIME_MS);
    
#endif
    
        error = DS18B20_RunConversion(&_ds18b20Handle, pAddress, NULL);
        
        if (error != OW_ERR_OK)
        {
            return;
        }
        
        HAL_Delay(DS18B20_TEMP_CONV_TIME_RES_12_MS + 500);
        
        
        error = DS18B20_ReadData(&_ds18b20Handle, pAddress, &data, NULL);
        
        if (error != OW_ERR_OK)
        {
            return;
        }
        
        _temperature = data.Temperature;
        
#ifdef OW_APP_USE_LOAD_SAVE
    
        data.AlarmTriggerH = 9;
        data.AlarmTriggerL = -9;
        data.Resolution = DS18B20_RESOLUTION_9;        
        
        error = DS18B20_WriteData(&_ds18b20Handle, pAddress, &data, NULL);
    
        if (error != OW_ERR_OK)
        {
            return;
        }
        
        
        error = DS18B20_ReadData(&_ds18b20Handle, pAddress, &data, NULL);
        
        if (error != OW_ERR_OK)
        {
            return;
        }
        
        
        error = DS18B20_LoadData(&_ds18b20Handle, pAddress, NULL);
        
        if (error != OW_ERR_OK)
        {
            return;
        }
        
        HAL_Delay(DS18B20_LOAD_TIME_MS);
        
        
        error = DS18B20_ReadData(&_ds18b20Handle, pAddress, &data, NULL);
        
        if (error != OW_ERR_OK)
        {
            return;
        }
#endif
        
#endif
}

void OW_APP_Async(void)
{
#ifdef OW_APP_ASYNC
    
    memset(&_ds18b20Handle, 0, sizeof(DS18B20_HandleTypeDef)); /* Clear for reuse. */
    
    OW_AddressTypeDef *pAddress = NULL;    
    
    OW_ErrorTypeDef error = OW_ERR_OK;

    
#ifdef OW_APP_USE_ADDRESS
    
    OW_AddressTypeDef address = {0};
    pAddress = &address;
    
    error = DS18B20_BeginReadAddress(&_ds18b20Handle, pAddress, NULL);
    
    if (error != OW_ERR_OK)
    {
        return;
    }
    
    do
    {
        error = DS18B20_CompleteReadAddress(&_ds18b20Handle);
        
        switch (error)
        {
            case OW_ERR_OK:
            case OW_ERR_NOT_COMPLETED:
                break;
            default:
                return;
        }
    }
    while (error == OW_ERR_NOT_COMPLETED);
    
#endif
    
    DS18B20_DataTypeDef data =
    {
        .Temperature = 0,        
        .AlarmTriggerH = DS18B20_MAX_ALARM_TRIGGER_H,
        .AlarmTriggerL = DS18B20_MIN_ALARM_TRIGGER_L,        
        .Resolution = DS18B20_RESOLUTION_12
    };
        
    error = DS18B20_BeginWriteData(&_ds18b20Handle, pAddress, &data, NULL);
    
    if (error != OW_ERR_OK)
    {
        return;
    }
    
    do
    {
        error = DS18B20_CompleteWriteData(&_ds18b20Handle);
        
        switch (error)
        {
            case OW_ERR_OK:
            case OW_ERR_NOT_COMPLETED:
                break;
            default:
                return;
        }
    }
    while (error == OW_ERR_NOT_COMPLETED);
    
#ifdef OW_APP_USE_LOAD_SAVE
    
        error = DS18B20_BeginSaveData(&_ds18b20Handle, pAddress, NULL);
        
        if (error != OW_ERR_OK)
        {
            return;
        }
        
        do
        {
            error = DS18B20_CompleteSaveData(&_ds18b20Handle);
            
            switch (error)
            {
                case OW_ERR_OK:
                case OW_ERR_NOT_COMPLETED:
                    break;
                default:
                    return;
            }
        }
        while (error == OW_ERR_NOT_COMPLETED);
        
        HAL_Delay(DS18B20_SAVE_TIME_MS);
    
#endif
    
        error = DS18B20_BeginRunConversion(&_ds18b20Handle, pAddress, NULL);
        
        if (error != OW_ERR_OK)
        {
            return;
        }
        
        do
        {
            error = DS18B20_CompleteRunConversion(&_ds18b20Handle);
            
            switch (error)
            {
                case OW_ERR_OK:
                case OW_ERR_NOT_COMPLETED:
                    break;
                default:
                    return;
            }
        }
        while (error == OW_ERR_NOT_COMPLETED);
        
        HAL_Delay(DS18B20_TEMP_CONV_TIME_RES_12_MS);
        
        
        error = DS18B20_BeginReadData(&_ds18b20Handle, pAddress, NULL);
        
        if (error != OW_ERR_OK)
        {
            return;
        }
        
        do
        {
            error = DS18B20_CompleteReadData(&_ds18b20Handle, &data);
            
            switch (error)
            {
                case OW_ERR_OK:
                case OW_ERR_NOT_COMPLETED:
                    break;
                default:
                    return;
            }
        }
        while (error == OW_ERR_NOT_COMPLETED);
        
        _temperature = data.Temperature;
        
#ifdef OW_APP_USE_LOAD_SAVE
    
        data.AlarmTriggerH = 9;
        data.AlarmTriggerL = -9;
        data.Resolution = DS18B20_RESOLUTION_9;        
        
        error = DS18B20_BeginWriteData(&_ds18b20Handle, pAddress, &data, NULL);
    
        if (error != OW_ERR_OK)
        {
            return;
        }
        
        do
        {
            error = DS18B20_CompleteWriteData(&_ds18b20Handle);
            
            switch (error)
            {
                case OW_ERR_OK:
                case OW_ERR_NOT_COMPLETED:
                    break;
                default:
                    return;
            }
        }
        while (error == OW_ERR_NOT_COMPLETED);
        
        
        error = DS18B20_BeginReadData(&_ds18b20Handle, pAddress, NULL);
        
        if (error != OW_ERR_OK)
        {
            return;
        }
        
        do
        {
            error = DS18B20_CompleteReadData(&_ds18b20Handle, &data);
            
            switch (error)
            {
                case OW_ERR_OK:
                case OW_ERR_NOT_COMPLETED:
                    break;
                default:
                    return;
            }
        }
        while (error == OW_ERR_NOT_COMPLETED);
        
        
        error = DS18B20_BeginLoadData(&_ds18b20Handle, pAddress, NULL);
        
        if (error != OW_ERR_OK)
        {
            return;
        }
        
        do
        {
            error = DS18B20_CompleteLoadData(&_ds18b20Handle);
            
            switch (error)
            {
                case OW_ERR_OK:
                case OW_ERR_NOT_COMPLETED:
                    break;
                default:
                    return;
            }
        }
        while (error == OW_ERR_NOT_COMPLETED);
        
        HAL_Delay(DS18B20_LOAD_TIME_MS);
        
        
        error = DS18B20_BeginReadData(&_ds18b20Handle, pAddress, NULL);
        
        if (error != OW_ERR_OK)
        {
            return;
        }
        
        do
        {
            error = DS18B20_CompleteReadData(&_ds18b20Handle, &data);
            
            switch (error)
            {
                case OW_ERR_OK:
                case OW_ERR_NOT_COMPLETED:
                    break;
                default:
                    return;
            }
        }
        while (error == OW_ERR_NOT_COMPLETED);
#endif

#endif
}
