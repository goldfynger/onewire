#include <stdbool.h>
#include <stdint.h>
#include "stm32f0xx_hal.h"
#include "onewire.h"
#include "board.h"


extern UART_HandleTypeDef huart1;

static volatile bool _txCompleted;
static volatile bool _rxCompleted;
static volatile bool _txRxError;


/* Weak callback. */
OW_ErrorTypeDef OW_HAL_ToInitSpeed(void *pHalContext)
{
    (void)pHalContext;
    
    huart1.Init.BaudRate = 9600;
    
    return (HAL_UART_Init(&huart1) == HAL_OK) ? OW_ERR_OK : OW_ERR_HAL;
}

/* Weak callback. */
OW_ErrorTypeDef OW_HAL_ToWorkSpeed(void *pHalContext)
{
    (void)pHalContext;
    
    huart1.Init.BaudRate = 115200;
    
    return (HAL_UART_Init(&huart1) == HAL_OK) ? OW_ERR_OK : OW_ERR_HAL;
}

/* Weak callback. */
OW_ErrorTypeDef OW_HAL_Exchange(void *pHalContext, uint8_t *pTxData, uint8_t *pRxData, uint8_t length)
{
    (void)pHalContext;
    
    _txCompleted = false;
    _rxCompleted = false;
    _txRxError = false;
    
    if (HAL_UART_Transmit_IT(&huart1, pTxData, length) != HAL_OK) return OW_ERR_HAL;
    if (HAL_UART_Receive_IT(&huart1, pRxData, length) != HAL_OK) return OW_ERR_HAL;
    
    while (true)
    {
        if (_txRxError) return OW_ERR_HAL;
        
        if (_txCompleted && _rxCompleted) return OW_ERR_OK;
    }
}

/* Weak callback. */
OW_ErrorTypeDef OW_HAL_BeginExchange(void *pHalContext, uint8_t *pTxData, uint8_t *pRxData, uint8_t length)
{
    (void)pHalContext;
    
    _txCompleted = false;
    _rxCompleted = false;
    _txRxError = false;
    
    if (HAL_UART_Transmit_IT(&huart1, pTxData, length) != HAL_OK) return OW_ERR_HAL;
    if (HAL_UART_Receive_IT(&huart1, pRxData, length) != HAL_OK) return OW_ERR_HAL;
    
    return OW_ERR_OK;
}

/* Weak callback. */
OW_ErrorTypeDef OW_HAL_EndExchange(void *pHalContext)
{
    (void)pHalContext;
    
    if (_txRxError) return OW_ERR_HAL;
    
    return (!_txCompleted || !_rxCompleted) ? OW_ERR_NOT_COMPLETED : OW_ERR_OK;
}


/* Weak callback. */
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
    _txCompleted = true;
}

/* Weak callback. */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
    _rxCompleted = true;
}

/* Weak callback. */
void HAL_UART_ErrorCallback(UART_HandleTypeDef *huart)
{
    _txRxError = true;
}
