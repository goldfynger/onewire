#ifndef __DS18B20_H
#define __DS18B20_H


#include <stdint.h>
#include "onewire.h"


#define DS18B20_BUFFER_LEN                      ((uint8_t)9)    /* Length of scratchpad. */

#define DS18B20_MAX_ALARM_TRIGGER_H             (INT8_MAX)
#define DS18B20_MIN_ALARM_TRIGGER_L             (INT8_MIN)

#define DS18B20_TEMP_CONV_TIME_RES_9_MS         ((uint32_t)94)  /* 93.75 */
#define DS18B20_TEMP_CONV_TIME_RES_10_MS        ((uint32_t)188) /* 187.5 */
#define DS18B20_TEMP_CONV_TIME_RES_11_MS        ((uint32_t)375)
#define DS18B20_TEMP_CONV_TIME_RES_12_MS        ((uint32_t)750)

#define DS18B20_LOAD_TIME_MS                    ((uint32_t)100) /* Theoretical value. */
#define DS18B20_SAVE_TIME_MS                    ((uint32_t)100) /* Theoretical value. */

#define DS18B20_CONFIG_BASE                     ((uint8_t)0x1F)
#define DS18B20_CONFIG_R0                       ((uint8_t)5)
#define DS18B20_CONFIG_R1                       ((uint8_t)6)


typedef enum
{
    DS18B20_RESOLUTION_9    = ((uint8_t)(0 << DS18B20_CONFIG_R0 | 0 << DS18B20_CONFIG_R1 | DS18B20_CONFIG_BASE)),
    DS18B20_RESOLUTION_10   = ((uint8_t)(1 << DS18B20_CONFIG_R0 | 0 << DS18B20_CONFIG_R1 | DS18B20_CONFIG_BASE)),
    DS18B20_RESOLUTION_11   = ((uint8_t)(0 << DS18B20_CONFIG_R0 | 1 << DS18B20_CONFIG_R1 | DS18B20_CONFIG_BASE)),
    DS18B20_RESOLUTION_12   = ((uint8_t)(1 << DS18B20_CONFIG_R0 | 1 << DS18B20_CONFIG_R1 | DS18B20_CONFIG_BASE)),
}
DS18B20_ResolutionTypeDef;


typedef struct
{
    int32_t                     Temperature;    /* 4 | m�C */
    
    int8_t                      AlarmTriggerH;  /* 5 */
    int8_t                      AlarmTriggerL;  /* 6 */   
    
    DS18B20_ResolutionTypeDef   Resolution;     /* 7 */
}
DS18B20_DataTypeDef;

typedef struct
{
    OW_HandleTypeDef            OwHandle;                   /* 32 */

    uint8_t                     Buffer[DS18B20_BUFFER_LEN]; /* 41 */
}
DS18B20_HandleTypeDef;


OW_ErrorTypeDef DS18B20_ReadAddress(DS18B20_HandleTypeDef *hDs18b20, OW_AddressTypeDef *pAddress, void *pHalContext);
OW_ErrorTypeDef DS18B20_BeginReadAddress(DS18B20_HandleTypeDef *hDs18b20, OW_AddressTypeDef *pAddress, void *pHalContext);
OW_ErrorTypeDef DS18B20_CompleteReadAddress(DS18B20_HandleTypeDef *hDs18b20);
OW_ErrorTypeDef DS18B20_RunConversion(DS18B20_HandleTypeDef *hDs18b20, OW_AddressTypeDef *pAddress, void *pHalContext);
OW_ErrorTypeDef DS18B20_BeginRunConversion(DS18B20_HandleTypeDef *hDs18b20, OW_AddressTypeDef *pAddress, void *pHalContext);
OW_ErrorTypeDef DS18B20_CompleteRunConversion(DS18B20_HandleTypeDef *hDs18b20);
OW_ErrorTypeDef DS18B20_ReadData(DS18B20_HandleTypeDef *hDs18b20, OW_AddressTypeDef *pAddress, DS18B20_DataTypeDef *pData, void *pHalContext);
OW_ErrorTypeDef DS18B20_BeginReadData(DS18B20_HandleTypeDef *hDs18b20, OW_AddressTypeDef *pAddress, void *pHalContext);
OW_ErrorTypeDef DS18B20_CompleteReadData(DS18B20_HandleTypeDef *hDs18b20, DS18B20_DataTypeDef *pData);
OW_ErrorTypeDef DS18B20_WriteData(DS18B20_HandleTypeDef *hDs18b20, OW_AddressTypeDef *pAddress, DS18B20_DataTypeDef *pData, void *pHalContext);
OW_ErrorTypeDef DS18B20_BeginWriteData(DS18B20_HandleTypeDef *hDs18b20, OW_AddressTypeDef *pAddress, DS18B20_DataTypeDef *pData, void *pHalContext);
OW_ErrorTypeDef DS18B20_CompleteWriteData(DS18B20_HandleTypeDef *hDs18b20);
OW_ErrorTypeDef DS18B20_LoadData(DS18B20_HandleTypeDef *hDs18b20, OW_AddressTypeDef *pAddress, void *pHalContext);
OW_ErrorTypeDef DS18B20_BeginLoadData(DS18B20_HandleTypeDef *hDs18b20, OW_AddressTypeDef *pAddress, void *pHalContext);
OW_ErrorTypeDef DS18B20_CompleteLoadData(DS18B20_HandleTypeDef *hDs18b20);
OW_ErrorTypeDef DS18B20_SaveData(DS18B20_HandleTypeDef *hDs18b20, OW_AddressTypeDef *pAddress, void *pHalContext);
OW_ErrorTypeDef DS18B20_BeginSaveData(DS18B20_HandleTypeDef *hDs18b20, OW_AddressTypeDef *pAddress, void *pHalContext);
OW_ErrorTypeDef DS18B20_CompleteSaveData(DS18B20_HandleTypeDef *hDs18b20);


#endif /* __DS18B20_H */
