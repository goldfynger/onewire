#ifndef __ONEWIRE_H
#define __ONEWIRE_H


#include <stdbool.h>
#include <stdint.h>


#define OW_ADDR_LENGTH          ((uint8_t)8)
#define OW_BYTE_LENGTH          ((uint8_t)8)


typedef enum
{
    OW_ERR_OK                   = ((uint8_t)0x00),
    
    OW_ERR_INVALID_PARAMETER,
    OW_ERR_INVALID_COMMAND,
    OW_ERR_INVALID_ADDRESS,
    OW_ERR_INVALID_CRC,
    
    OW_ERR_NO_PRESENCE,
    OW_ERR_COLLISION,
    OW_ERR_HAL,
    
    OW_ERR_BUSY,
    OW_ERR_NOT_BUSY,
    OW_ERR_INVALID_STATE,
    OW_ERR_INVALID_OPERATION,
    OW_ERR_NOT_COMPLETED,    
    
    OW_ERR_NOT_IMPLEMENTED,
}
OW_ErrorTypeDef;

typedef enum
{
    OW_CMD_SEARCHROM            = 0xF0, /* Not implemented. */
    OW_CMD_READROM              = 0x33, /* Reads address in Address field. */
    OW_CMD_MATCHROM             = 0x55, /* Not implemented for directly use. Used when Address field not NULL. */
    OW_CMD_SKIPROM              = 0xCC, /* Not implemented for directly use. Used when Address field is NULL. */
    OW_CMD_ALARMSEARCH          = 0xEC, /* Not implemented. */

    OW_CMD_CONVERTT             = 0x44, /* Runs temperature conversion. */

    OW_CMD_WRITESCRATCHPAD      = 0x4E, /* Writes data into internal scratchpad. */
    OW_CMD_READSCRATCHPAD       = 0xBE, /* Reads data from internal scratchpad. */
    OW_CMD_COPYSCRATCHPAD       = 0x48, /* Copies data from internal scratchpad into internal EEPROM. */
    OW_CMD_RECALLE              = 0xB8, /* Reads data from internal EEPROM into internal sctratchpad. */
    OW_CMD_READPOWERSUPPLY      = 0xB4, /* Not implemented. */
}
OW_CmdTypeDef;

typedef enum
{
    OW_FLAG_NONE                = 0,

    OW_FLAG_STATE_IDLE          = (0 << 0),
    OW_FLAG_STATE_WAIT_PRESENCE = (1 << 0),
    OW_FLAG_STATE_MATCHROM      = (2 << 0),
    OW_FLAG_STATE_SKIPROM       = (3 << 0),
    OW_FLAG_STATE_CMD           = (4 << 0),

    __OW_FLAG_STATE_MASK        = (7 << 0),

    OW_FLAG_BUSY                = (1 << 4),
}
OW_FlagTypeDef;


typedef struct
{
    uint8_t Array[OW_ADDR_LENGTH];
}
OW_AddressTypeDef;

typedef struct
{
    uint8_t Bits[OW_BYTE_LENGTH];
}
OW_HAL_ByteTypeDef;

typedef struct
{
    OW_HAL_ByteTypeDef  TxBuffer;       /* 8  | Temporary buffer for data writing. */
    OW_HAL_ByteTypeDef  RxBuffer;       /* 16 | Temporary buffer for data reading. */
    
    void              * HalContext;     /* 20 | User defined HAL context pointer. */
    
    OW_AddressTypeDef * Address;        /* 24 | Device address buffer pointer. Can be NULL if address not used or empty before reading. */
    
    uint8_t           * Buffer;         /* 28 | Pointer at external buffer for data reading or writing. */    
    uint8_t             BufferLength;   /* 29 | Length of buffer and length of data to read or write. */
    
    uint8_t             RxTxIndex;      /* 30 | Current index of read or write. Used for Address and Buffer fields. */
    
    OW_CmdTypeDef       Command;        /* 31 | Current user command. */
    
    OW_FlagTypeDef      Flag;           /* 32 | OW flags. */
}
OW_HandleTypeDef;


OW_ErrorTypeDef OW_Execute          (OW_HandleTypeDef *hOw, OW_CmdTypeDef command, OW_AddressTypeDef *pAddress, uint8_t *pTxRxData, uint8_t length, void *pHalContext);
OW_ErrorTypeDef OW_BeginExecute     (OW_HandleTypeDef *hOw, OW_CmdTypeDef command, OW_AddressTypeDef *pAddress, uint8_t *pTxRxData, uint8_t length, void *pHalContext);
OW_ErrorTypeDef OW_CompleteExecute  (OW_HandleTypeDef *hOw);

uint8_t         OW_CrcCalculate     (uint8_t *pData, uint8_t length);
uint8_t         OW_CrcAccumulate    (uint8_t crc, uint8_t *pData, uint8_t length);

/* __weak functions. */
OW_ErrorTypeDef OW_HAL_ToInitSpeed  (void *pHalContext);
OW_ErrorTypeDef OW_HAL_ToWorkSpeed  (void *pHalContext);
OW_ErrorTypeDef OW_HAL_Exchange     (void *pHalContext, uint8_t *pTxData, uint8_t *pRxData, uint8_t length);
OW_ErrorTypeDef OW_HAL_BeginExchange(void *pHalContext, uint8_t *pTxData, uint8_t *pRxData, uint8_t length);
OW_ErrorTypeDef OW_HAL_EndExchange  (void *pHalContext);


#endif /* __ONEWIRE_H */
