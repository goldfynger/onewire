#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include "onewire.h"


#define OW_INITIAL_BYTE         ((uint8_t)0xF0)
#define OW_BYTE_TO_READ         ((uint8_t)0xFF)

#define OW_ADDR_CRC_INDEX       ((uint8_t)7)
#define OW_ADDR_CRC_BASE_LENGTH ((uint8_t)7)


typedef enum
{
    OW_BIT_0    = 0x00,
    OW_BIT_1    = 0xFF,
}
OW_BitTypeDef;


static OW_ErrorTypeDef  OW_Reset        (OW_HandleTypeDef *hOw);
static OW_ErrorTypeDef  OW_BeginReset   (OW_HandleTypeDef *hOw);
static OW_ErrorTypeDef  OW_EndReset     (OW_HandleTypeDef *hOw);
static OW_ErrorTypeDef  OW_Write        (OW_HandleTypeDef *hOw, uint8_t txByte);
static OW_ErrorTypeDef  OW_BeginWrite   (OW_HandleTypeDef *hOw, uint8_t txByte);
static OW_ErrorTypeDef  OW_EndWrite     (OW_HandleTypeDef *hOw);
static OW_ErrorTypeDef  OW_Read         (OW_HandleTypeDef *hOw, uint8_t *pRxByte);
static OW_ErrorTypeDef  OW_BeginRead    (OW_HandleTypeDef *hOw);
static OW_ErrorTypeDef  OW_EndRead      (OW_HandleTypeDef *hOw, uint8_t *pRxByte);
static void             OW_ByteToTx     (uint8_t byte, OW_HAL_ByteTypeDef *pTxBuffer);
static uint8_t          OW_RxToByte     (OW_HAL_ByteTypeDef *pRxBuffer);
static void             OW_SetState     (OW_HandleTypeDef *hOw, OW_FlagTypeDef flag);
static OW_FlagTypeDef   OW_GetState     (OW_HandleTypeDef *hOw);


/* Executes command in sync mode. Address can be NULL if not used. */
OW_ErrorTypeDef OW_Execute(OW_HandleTypeDef *hOw, OW_CmdTypeDef command, OW_AddressTypeDef *pAddress, uint8_t *pTxRxData, uint8_t length, void *pHalContext)
{
    if (hOw == NULL) return OW_ERR_INVALID_PARAMETER;
    
    if (hOw->Flag & OW_FLAG_BUSY) return OW_ERR_BUSY;
    
    hOw->HalContext = pHalContext;

    switch (command)
    {
        case OW_CMD_READROM:
        {
            if (pAddress == NULL) return OW_ERR_INVALID_ADDRESS; /* Used for address reading. */
        } /* No break! */
        case OW_CMD_CONVERTT:
        case OW_CMD_COPYSCRATCHPAD:
        case OW_CMD_RECALLE:
        break; /* No actions needed. */

        case OW_CMD_WRITESCRATCHPAD:
        case OW_CMD_READSCRATCHPAD:
        {
            if (pTxRxData == NULL) return OW_ERR_INVALID_PARAMETER;
            if (length == 0) return OW_ERR_INVALID_PARAMETER;
        }
        break;

        case OW_CMD_SEARCHROM:
        case OW_CMD_MATCHROM:
        case OW_CMD_SKIPROM:
        case OW_CMD_ALARMSEARCH:
        case OW_CMD_READPOWERSUPPLY:
        {
            return OW_ERR_NOT_IMPLEMENTED;
        }

        default: return OW_ERR_INVALID_COMMAND;
    }

    OW_ErrorTypeDef error = OW_ERR_OK;

    /* Send reset and wait presence. */
    if ((error = OW_Reset(hOw)) != OW_ERR_OK) return error;

    /* Skip or match address sending. */
    if (command != OW_CMD_READROM)
    {
        OW_CmdTypeDef matchOrSkip;

        if (pAddress == NULL)
        {
            matchOrSkip = OW_CMD_SKIPROM;
        }
        else
        {
            matchOrSkip = OW_CMD_MATCHROM;
        }

        if ((error = OW_Write(hOw, matchOrSkip)) != OW_ERR_OK) return error;

        if (matchOrSkip == OW_CMD_MATCHROM)
        {
            for (uint8_t idx = 0; idx < OW_ADDR_LENGTH; idx++)
            {
                if ((error = OW_Write(hOw, pAddress->Array[idx])) != OW_ERR_OK) return error;
            }
        }
    }

    /* Write command. */
    if ((error = OW_Write(hOw, command)) != OW_ERR_OK) return error;

    switch (command)
    {
        case OW_CMD_READROM:
        {
            for (uint8_t idx = 0; idx < OW_ADDR_LENGTH; idx++)
            {
                if ((error = OW_Read(hOw, &pAddress->Array[idx])) != OW_ERR_OK) return error;
            }

            return pAddress->Array[OW_ADDR_CRC_INDEX] == OW_CrcCalculate(pAddress->Array, OW_ADDR_CRC_BASE_LENGTH) ? OW_ERR_OK : OW_ERR_INVALID_CRC;
        }

        case OW_CMD_WRITESCRATCHPAD:
        {
            for (uint8_t idx = 0; idx < length; idx++)
            {
                if ((error = OW_Write(hOw, pTxRxData[idx])) != OW_ERR_OK) return error;
            }
            
            return OW_ERR_OK;
        }

        case OW_CMD_READSCRATCHPAD:
        {
            for (uint8_t idx = 0; idx < length; idx++)
            {
                if ((error = OW_Read(hOw, &pTxRxData[idx])) != OW_ERR_OK) return error;
            }

            return OW_ERR_OK;
        }

        default: return OW_ERR_OK;
    }
}

/* Begins command executing in async mode. Address can be NULL if not used. */
OW_ErrorTypeDef OW_BeginExecute(OW_HandleTypeDef *hOw, OW_CmdTypeDef command, OW_AddressTypeDef *pAddress, uint8_t *pTxRxData, uint8_t length, void *pHalContext)
{
    if (hOw == NULL) return OW_ERR_INVALID_PARAMETER;
    
    if (hOw->Flag & OW_FLAG_BUSY) return OW_ERR_BUSY;
    
    hOw->HalContext = pHalContext;
    
    switch (command)
    {
        case OW_CMD_READROM:
        {
            if (pAddress == NULL) return OW_ERR_INVALID_ADDRESS; /* Used for address reading. */
        } /* No break! */
        case OW_CMD_CONVERTT:
        case OW_CMD_COPYSCRATCHPAD:
        case OW_CMD_RECALLE:
        {
            hOw->Command = command;
            hOw->Flag |= OW_FLAG_BUSY;
            hOw->Address = pAddress;
        }
        break;

        case OW_CMD_WRITESCRATCHPAD:
        case OW_CMD_READSCRATCHPAD:
        {
            if (pTxRxData == NULL) return OW_ERR_INVALID_PARAMETER;
            if (length == 0) return OW_ERR_INVALID_PARAMETER;
            
            hOw->Command = command;
            hOw->Flag |= OW_FLAG_BUSY;
            hOw->Address = pAddress;
            hOw->Buffer = pTxRxData;
            hOw->BufferLength = length;
        }
        break;

        case OW_CMD_SEARCHROM:
        case OW_CMD_MATCHROM:
        case OW_CMD_SKIPROM:
        case OW_CMD_ALARMSEARCH:
        case OW_CMD_READPOWERSUPPLY:
        {
            return OW_ERR_NOT_IMPLEMENTED;
        }

        default: return OW_ERR_INVALID_COMMAND;
    }
    
    return OW_BeginReset(hOw);
}

/* Process command executing in async mode. Returns OW_ERR_NOT_COMPLETED or OW_ERR_OK. */
OW_ErrorTypeDef OW_CompleteExecute(OW_HandleTypeDef *hOw)
{
    if (hOw == NULL) return OW_ERR_INVALID_PARAMETER;
    
    if (!(hOw->Flag & OW_FLAG_BUSY)) return OW_ERR_NOT_BUSY;
    
    OW_ErrorTypeDef error = OW_ERR_OK;
    
    switch (OW_GetState(hOw))
    {
        case OW_FLAG_STATE_WAIT_PRESENCE:
        {
            if ((error = OW_EndReset(hOw)) != OW_ERR_OK)
            {
                if (error != OW_ERR_NOT_COMPLETED) hOw->Flag &= ~OW_FLAG_BUSY;
                
                return error;
            }
            
            OW_CmdTypeDef commandToWrite;
            
            switch (hOw->Command)
            {
                case OW_CMD_READROM:
                {
                    OW_SetState(hOw, OW_FLAG_STATE_CMD);
                    commandToWrite = OW_CMD_READROM;
                }
                break;

                case OW_CMD_CONVERTT:
                case OW_CMD_WRITESCRATCHPAD:
                case OW_CMD_READSCRATCHPAD:
                case OW_CMD_COPYSCRATCHPAD:
                case OW_CMD_RECALLE:
                {
                    OW_SetState(hOw, hOw->Address != NULL ? OW_FLAG_STATE_MATCHROM : OW_FLAG_STATE_SKIPROM);                    
                    commandToWrite = hOw->Address != NULL ? OW_CMD_MATCHROM : OW_CMD_SKIPROM;
                }
                break;
                
                default:
                {
                    hOw->Flag &= ~OW_FLAG_BUSY;
                    return OW_ERR_INVALID_COMMAND;
                }
            }
            
            hOw->RxTxIndex = 0;
            
            if ((error = OW_BeginWrite(hOw, commandToWrite)) != OW_ERR_OK)
            {
                hOw->Flag &= ~OW_FLAG_BUSY;
                return error;
            }
            
            return OW_ERR_NOT_COMPLETED;
        }
        
        case OW_FLAG_STATE_MATCHROM:
        case OW_FLAG_STATE_SKIPROM:
        {
            if ((error = OW_EndWrite(hOw)) != OW_ERR_OK)
            {
                if (error != OW_ERR_NOT_COMPLETED) hOw->Flag &= ~OW_FLAG_BUSY;
                
                return error;
            }
            
            if (OW_GetState(hOw) == OW_FLAG_STATE_MATCHROM && hOw->RxTxIndex < OW_ADDR_LENGTH)
            {
                if ((error = OW_BeginWrite(hOw, hOw->Address->Array[hOw->RxTxIndex])) != OW_ERR_OK)
                {
                    hOw->Flag &= ~OW_FLAG_BUSY;
                    return error;
                }
                
                hOw->RxTxIndex++;
                return OW_ERR_NOT_COMPLETED;
            }
            else
            {
                OW_SetState(hOw, OW_FLAG_STATE_CMD);
                
                hOw->RxTxIndex = 0;
            
                if ((error = OW_BeginWrite(hOw, hOw->Command)) != OW_ERR_OK)
                {
                    hOw->Flag &= ~OW_FLAG_BUSY;
                    return error;
                }
                
                return OW_ERR_NOT_COMPLETED;
            }
        }
        
        case OW_FLAG_STATE_CMD:
        {
            switch (hOw->Command)
            {
                case OW_CMD_READROM:
                case OW_CMD_READSCRATCHPAD:
                {
                    if (hOw->RxTxIndex == 0) /* Ends command writing, begin reading data. */
                    {
                        if ((error = OW_EndWrite(hOw)) != OW_ERR_OK)
                        {
                            if (error != OW_ERR_NOT_COMPLETED) hOw->Flag &= ~OW_FLAG_BUSY;
                            
                            return error;
                        }
                        
                        if ((error = OW_BeginRead(hOw)) != OW_ERR_OK)
                        {
                            hOw->Flag &= ~OW_FLAG_BUSY;
                            return error;
                        }
                        
                        hOw->RxTxIndex++;
                        
                        return OW_ERR_NOT_COMPLETED;
                    }
                    else /* Continue reading data. */
                    {
                        uint8_t rxByte;
                        
                        if ((error = OW_EndRead(hOw, &rxByte)) != OW_ERR_OK)
                        {
                            if (error != OW_ERR_NOT_COMPLETED) hOw->Flag &= ~OW_FLAG_BUSY;
                            
                            return error;
                        }
                        
                        uint8_t bytesToRead;
                        
                        if (hOw->Command == OW_CMD_READROM)
                        {
                            bytesToRead = OW_ADDR_LENGTH;
                            hOw->Address->Array[hOw->RxTxIndex - 1] = rxByte;
                        }
                        else
                        {
                            bytesToRead = hOw->BufferLength;
                            hOw->Buffer[hOw->RxTxIndex - 1] = rxByte;
                        }
                        
                        if (bytesToRead > (hOw->RxTxIndex)) /* Continue reading data. */
                        {
                            if ((error = OW_BeginRead(hOw)) != OW_ERR_OK)
                            {
                                hOw->Flag &= ~OW_FLAG_BUSY;
                                return error;
                            }
                            
                            hOw->RxTxIndex++;
                            
                            return OW_ERR_NOT_COMPLETED;
                        }
                        else /* Ends reading data. */
                        {
                            hOw->Flag &= ~OW_FLAG_BUSY;
                            
                            if (hOw->Command == OW_CMD_READROM)
                            {
                                if (hOw->Address->Array[OW_ADDR_CRC_INDEX] != OW_CrcCalculate(hOw->Address->Array, OW_ADDR_CRC_BASE_LENGTH)) return OW_ERR_INVALID_CRC;
                            }
                            
                            return OW_ERR_OK;
                        }
                    }
                }
                
                case OW_CMD_CONVERTT:
                case OW_CMD_COPYSCRATCHPAD:
                case OW_CMD_RECALLE:
                case OW_CMD_WRITESCRATCHPAD:
                {
                    if ((error = OW_EndWrite(hOw)) != OW_ERR_OK)
                    {
                        if (error != OW_ERR_NOT_COMPLETED) hOw->Flag &= ~OW_FLAG_BUSY;
                        
                        return error;
                    }
                    
                    if (hOw->Command != OW_CMD_WRITESCRATCHPAD || hOw->RxTxIndex == hOw->BufferLength)
                    {
                        hOw->Flag &= ~OW_FLAG_BUSY;
                        
                        return OW_ERR_OK;
                    }
                    else /* Continue data writing. */
                    {
                        if ((error = OW_BeginWrite(hOw, hOw->Buffer[hOw->RxTxIndex])) != OW_ERR_OK)
                        {
                            hOw->Flag &= ~OW_FLAG_BUSY;
                            return error;
                        }
                        
                        hOw->RxTxIndex++;
                        return OW_ERR_NOT_COMPLETED;
                    }
                }
                
                default:
                {
                    hOw->Flag &= ~OW_FLAG_BUSY;
                    return OW_ERR_INVALID_COMMAND;
                }
            }
        }

        default: return OW_ERR_INVALID_STATE;
    }
}

/* Calculates CRC8 in MAXIM mode. */
uint8_t OW_CrcCalculate(uint8_t *pData, uint8_t length)
{
    return OW_CrcAccumulate(0, pData, length);
}

/* Accumulates CRC8 in MAXIM mode with previous calculated value. */
uint8_t OW_CrcAccumulate(uint8_t crc, uint8_t *pData, uint8_t length)
{
    for (uint8_t idx = 0; idx < length; idx++)
    {
       crc = crc ^ pData[idx];
       
       for (uint8_t i = 0; i < 8; i++)
       {
           if (crc & 0x01)
           {
               crc = (crc >> 1) ^ 0x8C;
           }
           else
           {
               crc >>= 1;
           }
       }
    }

    return crc;
}


/* Resets 1Wire in sync mode. */
static OW_ErrorTypeDef OW_Reset(OW_HandleTypeDef *hOw)
{
    OW_ErrorTypeDef error = OW_ERR_OK;

    if ((error = OW_HAL_ToInitSpeed(hOw->HalContext)) != OW_ERR_OK) return error;

    /* First bytes of RX and TX arrays used for sending reset and receiving presence pulses. */
    hOw->TxBuffer.Bits[0] = OW_INITIAL_BYTE;
    hOw->RxBuffer.Bits[0] = OW_INITIAL_BYTE;

    if ((error = OW_HAL_Exchange(hOw->HalContext, hOw->TxBuffer.Bits, hOw->RxBuffer.Bits, 1)) != OW_ERR_OK) return error;

    if (hOw->RxBuffer.Bits[0] == OW_INITIAL_BYTE) return OW_ERR_NO_PRESENCE;

    if ((error = OW_HAL_ToWorkSpeed(hOw->HalContext)) != OW_ERR_OK) return error;

    return OW_ERR_OK;
}

/* Begins reset 1Wire in async mode. */
static OW_ErrorTypeDef OW_BeginReset(OW_HandleTypeDef *hOw)
{
    OW_ErrorTypeDef error = OW_ERR_OK;

    if ((error = OW_HAL_ToInitSpeed(hOw->HalContext)) != OW_ERR_OK) return error;

    /* First bytes of RX and TX arrays used for sending reset and receiving presence pulses. */
    hOw->TxBuffer.Bits[0] = OW_INITIAL_BYTE;
    hOw->RxBuffer.Bits[0] = OW_INITIAL_BYTE;

    if ((error = OW_HAL_BeginExchange(hOw->HalContext, hOw->TxBuffer.Bits, hOw->RxBuffer.Bits, 1)) != OW_ERR_OK) return error;
    
    OW_SetState(hOw, OW_FLAG_STATE_WAIT_PRESENCE);

    return OW_ERR_OK;
}

/* Process reset 1Wire in async mode. Returns OW_ERR_NOT_COMPLETED or OW_ERR_OK. */
static OW_ErrorTypeDef OW_EndReset(OW_HandleTypeDef *hOw)
{
    OW_ErrorTypeDef error = OW_ERR_OK;
    
    if ((error = OW_HAL_EndExchange(hOw->HalContext)) != OW_ERR_OK) return error;
    
    if (hOw->RxBuffer.Bits[0] == OW_INITIAL_BYTE) return OW_ERR_NO_PRESENCE;
    
    if ((error = OW_HAL_ToWorkSpeed(hOw->HalContext)) != OW_ERR_OK) return error;

    return OW_ERR_OK;
}

/* Writes one byte in sync mode. */
static OW_ErrorTypeDef OW_Write(OW_HandleTypeDef *hOw, uint8_t txByte)
{
    OW_ErrorTypeDef error = OW_ERR_OK;

    OW_ByteToTx(txByte, &hOw->TxBuffer);

    memset(hOw->RxBuffer.Bits, 0, OW_BYTE_LENGTH);

    if ((error = OW_HAL_Exchange(hOw->HalContext, hOw->TxBuffer.Bits, hOw->RxBuffer.Bits, OW_BYTE_LENGTH)) != OW_ERR_OK) return error;

    if (memcmp(hOw->TxBuffer.Bits, hOw->RxBuffer.Bits, OW_BYTE_LENGTH) != 0) return OW_ERR_COLLISION;

    return OW_ERR_OK;
}

/* Begins write one byte in async mode. */
static OW_ErrorTypeDef OW_BeginWrite(OW_HandleTypeDef *hOw, uint8_t txByte)
{
    OW_ErrorTypeDef error = OW_ERR_OK;

    OW_ByteToTx(txByte, &hOw->TxBuffer);

    memset(hOw->RxBuffer.Bits, 0, OW_BYTE_LENGTH);
    
    if ((error = OW_HAL_BeginExchange(hOw->HalContext, hOw->TxBuffer.Bits, hOw->RxBuffer.Bits, OW_BYTE_LENGTH)) != OW_ERR_OK) return error;

    return OW_ERR_OK;
}

/* Process write one byte in async mode. Returns OW_ERR_NOT_COMPLETED or OW_ERR_OK. */
static OW_ErrorTypeDef OW_EndWrite(OW_HandleTypeDef *hOw)
{
    OW_ErrorTypeDef error = OW_ERR_OK;
    
    if ((error = OW_HAL_EndExchange(hOw->HalContext)) != OW_ERR_OK) return error;
    
    if (memcmp(hOw->TxBuffer.Bits, hOw->RxBuffer.Bits, OW_BYTE_LENGTH) != 0) return OW_ERR_COLLISION;
    
    return OW_ERR_OK;
}

/* Reads one byte in sync mode. */
static OW_ErrorTypeDef OW_Read(OW_HandleTypeDef *hOw, uint8_t *pRxByte)
{
    OW_ErrorTypeDef error = OW_ERR_OK;
    
    OW_ByteToTx(OW_BYTE_TO_READ, &hOw->TxBuffer);

    memset(hOw->RxBuffer.Bits, 0, OW_BYTE_LENGTH);

    if ((error = OW_HAL_Exchange(hOw->HalContext, hOw->TxBuffer.Bits, hOw->RxBuffer.Bits, OW_BYTE_LENGTH)) != OW_ERR_OK) return error;

    *pRxByte = OW_RxToByte(&hOw->RxBuffer);

    return OW_ERR_OK;
}

/* Begins read one byte in async mode. */
static OW_ErrorTypeDef OW_BeginRead(OW_HandleTypeDef *hOw)
{
    OW_ErrorTypeDef error = OW_ERR_OK;
    
    OW_ByteToTx(OW_BYTE_TO_READ, &hOw->TxBuffer);

    memset(hOw->RxBuffer.Bits, 0, OW_BYTE_LENGTH);

    if ((error = OW_HAL_BeginExchange(hOw->HalContext, hOw->TxBuffer.Bits, hOw->RxBuffer.Bits, OW_BYTE_LENGTH)) != OW_ERR_OK) return error;

    return OW_ERR_OK;
}

/* Process read one byte in async mode. Returns OW_ERR_NOT_COMPLETED or OW_ERR_OK. */
static OW_ErrorTypeDef OW_EndRead(OW_HandleTypeDef *hOw, uint8_t *pRxByte)
{
    OW_ErrorTypeDef error = OW_ERR_OK;

    if ((error = OW_HAL_EndExchange(hOw->HalContext)) != OW_ERR_OK) return error;
    
    *pRxByte = OW_RxToByte(&hOw->RxBuffer);
    
    return OW_ERR_OK;
}

/* Converts byte to TxBuffer. */
static void OW_ByteToTx(uint8_t byte, OW_HAL_ByteTypeDef *pTxBuffer)
{
    for (uint8_t idx = 0; idx < OW_BYTE_LENGTH; idx++)
    {
        pTxBuffer->Bits[idx] = (byte & (1 << idx)) ? OW_BIT_1 : OW_BIT_0;
    }
}

/* Converts RxBuffer to byte. */
static uint8_t OW_RxToByte(OW_HAL_ByteTypeDef *pRxBuffer)
{    
    uint8_t byte = 0;

    for (uint8_t idx = 0; idx < OW_BYTE_LENGTH; idx++)
    {
        if (pRxBuffer->Bits[idx] == OW_BIT_1) byte |= 1 << idx;
    }

    return byte;
}

/* Gets current state. */
static OW_FlagTypeDef OW_GetState(OW_HandleTypeDef *hOw)
{
    return (OW_FlagTypeDef)(hOw->Flag & __OW_FLAG_STATE_MASK);
}

/* Sets new state. */
static void OW_SetState(OW_HandleTypeDef *hOw, OW_FlagTypeDef flag)
{
    hOw->Flag = (OW_FlagTypeDef)((hOw->Flag & (~__OW_FLAG_STATE_MASK)) | flag);
}


__weak OW_ErrorTypeDef OW_HAL_ToInitSpeed(void *pHalContext)
{
    (void)pHalContext;
    return OW_ERR_NOT_IMPLEMENTED;
}

__weak OW_ErrorTypeDef OW_HAL_ToWorkSpeed(void *pHalContext)
{
    (void)pHalContext;
    return OW_ERR_NOT_IMPLEMENTED;
}

__weak OW_ErrorTypeDef OW_HAL_Exchange(void *pHalContext, uint8_t *pTxData, uint8_t *pRxData, uint8_t length)
{
    (void)pHalContext;
    (void)pTxData;
    (void)pRxData;
    (void)length;
    return OW_ERR_NOT_IMPLEMENTED;
}

__weak OW_ErrorTypeDef OW_HAL_BeginExchange(void *pHalContext, uint8_t *pTxData, uint8_t *pRxData, uint8_t length)
{
    (void)pHalContext;
    (void)pTxData;
    (void)pRxData;
    (void)length;
    return OW_ERR_NOT_IMPLEMENTED;
}

__weak OW_ErrorTypeDef OW_HAL_EndExchange(void *pHalContext)
{
    (void)pHalContext;
    return OW_ERR_NOT_IMPLEMENTED;
}
