#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include "onewire.h"
#include "ds18b20.h"


#define DS18B20_CRC_BASE_LENGTH         ((uint8_t)8)

#define DS18B20_WRITE_BASE              ((uint8_t)2)
#define DS18B20_WRITE_LENGTH            ((uint8_t)3)


typedef struct
{
    uint8_t                     TemperatureLsb;
    uint8_t                     TemperatureMsb;

    int8_t                      AlarmTriggerH;  /* Can be written. */
    int8_t                      AlarmTriggerL;  /* Can be written. */

    DS18B20_ResolutionTypeDef   Resolution;     /* Can be written. */

    uint8_t                     ReservedFF;
    uint8_t                     Reserved;
    uint8_t                     Reserved10;

    uint8_t                     Crc;
}
DS18B20_ScratchpadTypeDef;


OW_ErrorTypeDef DS18B20_ReadAddress(DS18B20_HandleTypeDef *hDs18b20, OW_AddressTypeDef *pAddress, void *pHalContext)
{
    if (hDs18b20 == NULL) return OW_ERR_INVALID_PARAMETER;
    
    if (pAddress == NULL) return OW_ERR_INVALID_ADDRESS;
    
    return OW_Execute(&hDs18b20->OwHandle, OW_CMD_READROM, pAddress, NULL, 0, pHalContext);
}

OW_ErrorTypeDef DS18B20_BeginReadAddress(DS18B20_HandleTypeDef *hDs18b20, OW_AddressTypeDef *pAddress, void *pHalContext)
{
    if (hDs18b20 == NULL) return OW_ERR_INVALID_PARAMETER;
    
    if (pAddress == NULL) return OW_ERR_INVALID_ADDRESS;
    
    return OW_BeginExecute(&hDs18b20->OwHandle, OW_CMD_READROM, pAddress, NULL, 0, pHalContext);
}

OW_ErrorTypeDef DS18B20_CompleteReadAddress(DS18B20_HandleTypeDef *hDs18b20)
{
    if (hDs18b20 == NULL) return OW_ERR_INVALID_PARAMETER;
    
    if (hDs18b20->OwHandle.Command != OW_CMD_READROM) return OW_ERR_INVALID_OPERATION;
    
    return OW_CompleteExecute(&hDs18b20->OwHandle);
}

OW_ErrorTypeDef DS18B20_RunConversion(DS18B20_HandleTypeDef *hDs18b20, OW_AddressTypeDef *pAddress, void *pHalContext)
{
    if (hDs18b20 == NULL) return OW_ERR_INVALID_PARAMETER;
    
    return OW_Execute(&hDs18b20->OwHandle, OW_CMD_CONVERTT, pAddress, NULL, 0, pHalContext);
}

OW_ErrorTypeDef DS18B20_BeginRunConversion(DS18B20_HandleTypeDef *hDs18b20, OW_AddressTypeDef *pAddress, void *pHalContext)
{
    if (hDs18b20 == NULL) return OW_ERR_INVALID_PARAMETER;
    
    return OW_BeginExecute(&hDs18b20->OwHandle, OW_CMD_CONVERTT, pAddress, NULL, 0, pHalContext);
}

OW_ErrorTypeDef DS18B20_CompleteRunConversion(DS18B20_HandleTypeDef *hDs18b20)
{
    if (hDs18b20 == NULL) return OW_ERR_INVALID_PARAMETER;
    
    if (hDs18b20->OwHandle.Command != OW_CMD_CONVERTT) return OW_ERR_INVALID_OPERATION;
    
    return OW_CompleteExecute(&hDs18b20->OwHandle);
}

OW_ErrorTypeDef DS18B20_ReadData(DS18B20_HandleTypeDef *hDs18b20, OW_AddressTypeDef *pAddress, DS18B20_DataTypeDef *pData, void *pHalContext)
{
    if (hDs18b20 == NULL) return OW_ERR_INVALID_PARAMETER;

    DS18B20_ScratchpadTypeDef *pScratchpad = (DS18B20_ScratchpadTypeDef *)hDs18b20->Buffer;
    
    OW_ErrorTypeDef error = OW_ERR_OK;

    if ((error = OW_Execute(&hDs18b20->OwHandle, OW_CMD_READSCRATCHPAD, pAddress, (uint8_t *)pScratchpad, sizeof(DS18B20_ScratchpadTypeDef), pHalContext)) != OW_ERR_OK) return error;
    
    if (pScratchpad->Crc != OW_CrcCalculate((uint8_t *)pScratchpad, DS18B20_CRC_BASE_LENGTH)) return OW_ERR_INVALID_CRC;
    
    int16_t temperatureDigital = (pScratchpad->TemperatureMsb << 8) | (pScratchpad->TemperatureLsb);
    
    bool temperatureIsNegative = temperatureDigital < 0;    
    
    int32_t temperatureInteger = (temperatureDigital >> 4) & 0xFF;
    
    if (temperatureIsNegative)
    {
        temperatureInteger |= 0xFFFFFF00; /* Add negative sign bits. */
    }

    temperatureInteger *= 1000;
    
    uint16_t temperatureFractional = temperatureDigital & 0x0F;

    bool fractionalIsEven = (temperatureFractional % 2) == 0;

    if (!fractionalIsEven)
    {
        temperatureFractional -= 1;
    }

    temperatureFractional /= 2;        
    temperatureFractional *= 125;

    if (!fractionalIsEven)
    {
        temperatureFractional += 63;
    }
    
    if (temperatureIsNegative)
    {
        temperatureFractional -=1;
    }

    pData->Temperature = temperatureInteger + temperatureFractional;
    
    pData->AlarmTriggerH = pScratchpad->AlarmTriggerH;
    pData->AlarmTriggerL = pScratchpad->AlarmTriggerL;
    
    pData->Resolution = pScratchpad->Resolution;
    
    return OW_ERR_OK;
}

OW_ErrorTypeDef DS18B20_BeginReadData(DS18B20_HandleTypeDef *hDs18b20, OW_AddressTypeDef *pAddress, void *pHalContext)
{
    if (hDs18b20 == NULL) return OW_ERR_INVALID_PARAMETER;

    DS18B20_ScratchpadTypeDef *pScratchpad = (DS18B20_ScratchpadTypeDef *)hDs18b20->Buffer;

    return OW_BeginExecute(&hDs18b20->OwHandle, OW_CMD_READSCRATCHPAD, pAddress, (uint8_t *)pScratchpad, sizeof(DS18B20_ScratchpadTypeDef), pHalContext);
}

OW_ErrorTypeDef DS18B20_CompleteReadData(DS18B20_HandleTypeDef *hDs18b20, DS18B20_DataTypeDef *pData)
{
    if (hDs18b20 == NULL) return OW_ERR_INVALID_PARAMETER;
    
    if (hDs18b20->OwHandle.Command != OW_CMD_READSCRATCHPAD) return OW_ERR_INVALID_OPERATION;

    DS18B20_ScratchpadTypeDef *pScratchpad = (DS18B20_ScratchpadTypeDef *)hDs18b20->Buffer;
    
    OW_ErrorTypeDef error = OW_ERR_OK;

    if ((error = OW_CompleteExecute(&hDs18b20->OwHandle)) != OW_ERR_OK) return error;
    
    if (pScratchpad->Crc != OW_CrcCalculate((uint8_t *)pScratchpad, DS18B20_CRC_BASE_LENGTH)) return OW_ERR_INVALID_CRC;
    
    int16_t temperatureDigital = (pScratchpad->TemperatureMsb << 8) | (pScratchpad->TemperatureLsb);

    int32_t temperatureInteger = ((temperatureDigital >> 4) & 0xFF) * 1000;

    uint8_t temperatureFractional = temperatureDigital & 0x0F;

    uint16_t temp = temperatureFractional;

    bool fractionalIsEven = (temp % 2) == 0;

    if (!fractionalIsEven)
    {
        temp -= 1;
    }

    temp /= 2;        
    temp *= 125;

    if (!fractionalIsEven)
    {
        temp += 63;
    }

    if (temperatureInteger < 0)
    {
        temperatureInteger -= temp;
    }
    else
    {
        temperatureInteger += temp;
    }

    pData->Temperature = temperatureInteger;
    
    pData->AlarmTriggerH = pScratchpad->AlarmTriggerH;
    pData->AlarmTriggerL = pScratchpad->AlarmTriggerL;
    
    pData->Resolution = pScratchpad->Resolution;
    
    return OW_ERR_OK;
}

OW_ErrorTypeDef DS18B20_WriteData(DS18B20_HandleTypeDef *hDs18b20, OW_AddressTypeDef *pAddress, DS18B20_DataTypeDef *pData, void *pHalContext)
{
    if (hDs18b20 == NULL) return OW_ERR_INVALID_PARAMETER;

    DS18B20_ScratchpadTypeDef *pScratchpad = (DS18B20_ScratchpadTypeDef *)hDs18b20->Buffer;
    
    pScratchpad->AlarmTriggerH = pData->AlarmTriggerH;
    pScratchpad->AlarmTriggerL = pData->AlarmTriggerL;
    
    pScratchpad->Resolution = pData->Resolution;

    return OW_Execute(&hDs18b20->OwHandle, OW_CMD_WRITESCRATCHPAD, pAddress, (uint8_t *)pScratchpad + DS18B20_WRITE_BASE, DS18B20_WRITE_LENGTH, pHalContext);
}

OW_ErrorTypeDef DS18B20_BeginWriteData(DS18B20_HandleTypeDef *hDs18b20, OW_AddressTypeDef *pAddress, DS18B20_DataTypeDef *pData, void *pHalContext)
{
    if (hDs18b20 == NULL) return OW_ERR_INVALID_PARAMETER;

    DS18B20_ScratchpadTypeDef *pScratchpad = (DS18B20_ScratchpadTypeDef *)hDs18b20->Buffer;
    
    pScratchpad->AlarmTriggerH = pData->AlarmTriggerH;
    pScratchpad->AlarmTriggerL = pData->AlarmTriggerL;
    
    pScratchpad->Resolution = pData->Resolution;

    return OW_BeginExecute(&hDs18b20->OwHandle, OW_CMD_WRITESCRATCHPAD, pAddress, (uint8_t *)pScratchpad + DS18B20_WRITE_BASE, DS18B20_WRITE_LENGTH, pHalContext);
}

OW_ErrorTypeDef DS18B20_CompleteWriteData(DS18B20_HandleTypeDef *hDs18b20)
{
    if (hDs18b20 == NULL) return OW_ERR_INVALID_PARAMETER;
    
    if (hDs18b20->OwHandle.Command != OW_CMD_WRITESCRATCHPAD) return OW_ERR_INVALID_OPERATION;
    
    return OW_CompleteExecute(&hDs18b20->OwHandle);
}

OW_ErrorTypeDef DS18B20_LoadData(DS18B20_HandleTypeDef *hDs18b20, OW_AddressTypeDef *pAddress, void *pHalContext)
{
    if (hDs18b20 == NULL) return OW_ERR_INVALID_PARAMETER;
    
    return OW_Execute(&hDs18b20->OwHandle, OW_CMD_RECALLE, pAddress, NULL, 0, pHalContext);
}

OW_ErrorTypeDef DS18B20_BeginLoadData(DS18B20_HandleTypeDef *hDs18b20, OW_AddressTypeDef *pAddress, void *pHalContext)
{
    if (hDs18b20 == NULL) return OW_ERR_INVALID_PARAMETER;
    
    return OW_BeginExecute(&hDs18b20->OwHandle, OW_CMD_RECALLE, pAddress, NULL, 0, pHalContext);
}

OW_ErrorTypeDef DS18B20_CompleteLoadData(DS18B20_HandleTypeDef *hDs18b20)
{
    if (hDs18b20 == NULL) return OW_ERR_INVALID_PARAMETER;
    
    if (hDs18b20->OwHandle.Command != OW_CMD_RECALLE) return OW_ERR_INVALID_OPERATION;
    
    return OW_CompleteExecute(&hDs18b20->OwHandle);
}

OW_ErrorTypeDef DS18B20_SaveData(DS18B20_HandleTypeDef *hDs18b20, OW_AddressTypeDef *pAddress, void *pHalContext)
{
    if (hDs18b20 == NULL) return OW_ERR_INVALID_PARAMETER;
    
    return OW_Execute(&hDs18b20->OwHandle, OW_CMD_COPYSCRATCHPAD, pAddress, NULL, 0, pHalContext);
}

OW_ErrorTypeDef DS18B20_BeginSaveData(DS18B20_HandleTypeDef *hDs18b20, OW_AddressTypeDef *pAddress, void *pHalContext)
{
    if (hDs18b20 == NULL) return OW_ERR_INVALID_PARAMETER;
    
    return OW_BeginExecute(&hDs18b20->OwHandle, OW_CMD_COPYSCRATCHPAD, pAddress, NULL, 0, pHalContext);
}

OW_ErrorTypeDef DS18B20_CompleteSaveData(DS18B20_HandleTypeDef *hDs18b20)
{
    if (hDs18b20 == NULL) return OW_ERR_INVALID_PARAMETER;
    
    if (hDs18b20->OwHandle.Command != OW_CMD_COPYSCRATCHPAD) return OW_ERR_INVALID_OPERATION;
    
    return OW_CompleteExecute(&hDs18b20->OwHandle);
}
